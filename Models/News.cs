﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class News
    {
        [Key]
        public int NewsID { get; set; }
        [Required]
        [StringLength(200)]
        public String NewsOfEvent { get; set; }
        [Required]
        public DateTime NewsDateCreated { get; set; }
        [Required]
        [StringLength(25)]
        public String NewsAdminName { get; set; }
    }
}