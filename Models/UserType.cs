﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public enum Type
    {
        chef, guest, member
    }
    public class UserType
    {
        [Key]
        public int TypeID { get; set; }
        [Required]
        public Type? Type { get; set; }
    }
}