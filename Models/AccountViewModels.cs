﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FinalProject.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        public class DateRangeAttribute : ValidationAttribute
        {
            
             public int FirstDateYears { get; set; }
             public int SecondDateYears { get; set; }

             public DateRangeAttribute()
             {
                 FirstDateYears = 100;
                 SecondDateYears = 21;
             }

             public override bool IsValid(object value)
             {
                 DateTime date = DateTime.Parse(value.ToString());
                 if (date >= DateTime.Now.AddYears(-FirstDateYears) && date <= DateTime.Now.AddYears(-SecondDateYears))
                 {
                     return true;
                 }
                 return false;
             }
                    
        }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "User Name")]
        public String Name { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        [DateRange(ErrorMessage = "You must be between 21 and 99 years old to register here.")]
        [Display(Name = "Date of Birth")]
        public DateTime BirthDate { get; set; }
        [StringLength(25)]
        public string City { get; set; }
        [StringLength(2, ErrorMessage = "Must be only the 2 digit code for the state.", MinimumLength = 2)]
        public string State { get; set; }
        [Range(1, 99999)]
        public int Zip { get; set; }
        [Range(1, 100)]
        [Display(Name = "Distance willing to travel")]
        public int DistTraveled { get; set; }
        [Display(Name = "Prefered Cuisine Type")]
        public string CuisinePref { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}