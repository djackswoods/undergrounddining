﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Event
    {
        public class DateRangeAttribute : ValidationAttribute
        {

            public int FirstDateYears { get; set; }
            public int SecondDateYears { get; set; }

            public DateRangeAttribute()
            {
                FirstDateYears = 1;
                
            }

            public override bool IsValid(object value)
            {
                DateTime date = DateTime.Parse(value.ToString());
                if (date > DateTime.Now && date <= DateTime.Now.AddYears(FirstDateYears))
                {
                    return true;
                }
                return false;
            }

        }


        [Key]
        public int EventID { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Chef Name:")]
        public String EventChefName { get; set; }
        [Required]
        [StringLength(300)]
        [Display(Name = "About the Event:")]
        public String EventDetails { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        [DateRange(ErrorMessage = "Your event must be in the future, and must be within a year from today's date")]
        [Display(Name = "Date of Event:")]
        public DateTime EventDate { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(50)]
        [Display(Name = "Event Hosts Email:")]
        public String EventEmailOfPoster { get; set; }
        [Required]
        [Range(1, 50)]
        [Display(Name = "How many invitees for Event:")]
        public int EventSize { get; set; }
        [Display(Name = "Is the Event full? :")]
        public bool EventFull { get; set; }

        public virtual UserType TypeID { get; set; }

    }
}