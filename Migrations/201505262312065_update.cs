namespace FinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContactEmails",
                c => new
                    {
                        mailID = c.Int(nullable: false, identity: true),
                        From = c.String(nullable: false),
                        To = c.String(nullable: false),
                        Subject = c.String(nullable: false),
                        Body = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.mailID);
            
            CreateTable(
                "dbo.Emailers",
                c => new
                    {
                        mailID = c.Int(nullable: false, identity: true),
                        From = c.String(nullable: false),
                        To = c.String(nullable: false),
                        Subject = c.String(nullable: false),
                        Body = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.mailID);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventID = c.Int(nullable: false, identity: true),
                        EventChefName = c.String(nullable: false, maxLength: 50),
                        EventDetails = c.String(nullable: false, maxLength: 300),
                        EventDate = c.DateTime(nullable: false),
                        EventEmailOfPoster = c.String(nullable: false, maxLength: 50),
                        EventSize = c.Int(nullable: false),
                        EventFull = c.Boolean(nullable: false),
                        TypeID_TypeID = c.Int(),
                    })
                .PrimaryKey(t => t.EventID)
                .ForeignKey("dbo.UserTypes", t => t.TypeID_TypeID)
                .Index(t => t.TypeID_TypeID);
            
            CreateTable(
                "dbo.UserTypes",
                c => new
                    {
                        TypeID = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TypeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "TypeID_TypeID", "dbo.UserTypes");
            DropIndex("dbo.Events", new[] { "TypeID_TypeID" });
            DropTable("dbo.UserTypes");
            DropTable("dbo.Events");
            DropTable("dbo.Emailers");
            DropTable("dbo.ContactEmails");
        }
    }
}
